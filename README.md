# Technic Machines Utilities

## Introduction

Collection of Minetest ingame codes to optimize your electric network. 

## Table of contents

* [Introduction](#introduction)
* [I. Prerequisites](#prerequisites)
* [II. Available programs](#available-programs)
  * [1. Converter Auto Supply](#converter-auto-supply)
  * [2. Power Units Management](#power-units-management)
* [III. License](#license)
* [IV. Credits](#credits)

## Prerequisites

* [Minetest](https://minetest.net/)
* [Technic](https://content.minetest.net/packages/RealBadAngel/technic/)
* [Mesecons](https://content.minetest.net/packages/Jeija/mesecons/)
* [Digilines](https://content.minetest.net/packages/Jeija/digilines/)

## Available programs

### Converter Auto Supply

**Source code :** https://gitlab.com/Athozus/technic-machines-utilities/-/blob/main/converter_auto_supply.lua

This program allows main network to convert into other voltage the less as possible. It takes into account the lost power due to conversion : by default it is 10%, but you should edit variable `efficiency` if you run your own version of `technic` mod.

Initially the program is written without mentioning the in/out voltages, but you could edit the variables with your own voltages. You could also change the `update_time` (in seconds) according to your needs. This time determinates the max time to update supply. Consider that under 1 second, your server may lag. If your technic version isn't too old, a security is added to prevent overloading.

The programs uses following digilines channels : `converter` for the supply converter, `switch` for the switching station.

Finally, if your converter is by default disabled for some reason, you can tell the program to enable it when it receives demand by editing `force_enable` to `true`. Else, the converter will sleep and wait your manually toggling before supplying your network.

For example, if your main network is HV, and you need 450 LV power, after 1 second the converter will receive 450/0.9 = 500 HV. When your LV machine will turn off, it will go back automaticly to 0.

**How to use it**

1. Place next to each other : 1 **supply converter**, 1 **switching station**. Be sure that your switching station is managing your secondary network.
2. Place 1 **lua controller** next to one of the previous machines.
3. Place 1 **digiline-wire** to connect micro-controller to your lastest machine.
4. Copy-paste the code into the controller.
5. Set up your digilines channels according to your desires.

### Power Units Management

**Source code :** https://gitlab.com/Athozus/technic-machines-utilities/-/blob/main/power_units_management.lua

**Note :** Currently the only way to make the system working (if you have more than one generator) is to build a double converter (for ex. HV -> LV -> HV). Then you have to connect the closest switching station to the generator with digilines. This annoying because it takes place and gives only 81% of the produced energy. This page be updated when a better way will be found.

***=> Consider that, until the solution for "segmentation" of networks isn't found, this program is not working. You could try it if you think you have the solution.***

This programs allows you to shows a summary of the generators of your electric networks. It shows, in a table, the channel of your power unit, its name (defined by you), its actual production compared to its maximum power, and its current activity (Stopped 0%, Awake 1-99%, Active 100%).

**How to use it**

To use it, place **switching stations** near your generators and assign them a digiline channel. Then connect with **digilines** to each other your switching stations. Near your digilines wire, put a **micro-controller** and a digiline **touchscreen**. Give to your touchscreen the channel `touchscreen`. Then copy-paste the source code into your micro-controllers. You can control the `update_time` (initially 2, to prevent overloading of the server). Then, you must add your generators just under the line creating `power_units`, following this syntax :

```lua
local power_units = {}
power_units["channel"] = {"Name", 0, max_power, toggleable}
```

To define the `toggleable` setting, just add `true`/`false` according to if your generator could be switched off. For example, a solar panel depends on the time of the day and not your will, so it has to be defined as `false`. But, a nuclear generator can be switched off, as you have to input the fuel inside it to work ; then, you have to define `toggleable` as `true`.

## License

This repo and all its programs are licensed in GPLv3.

## Credits

* [Athozus](https://gitlab.com/Athozus)
* ...and Minetest/mods contributors...
