local update_time = 2 -- time for loop
local power_units = {} -- ["digiline_channel"] = {"name", current_power (def 0), max_power, toggleable}

local function tableConcat(t1, t2)
    for i=1,#t2 do
        t1[#t1+1] = t2[i]
    end
    return t1
end

local function update_data()
    for unit_channel, data in pairs(power_units) do
        digiline_send(unit_channel, "get")
    end
end

if event.type == "program" then
	-- start interrupt cycle
	interrupt(update_time)
end

if event.type == "interrupt" then
    update_data()
	interrupt(update_time)
end

if event.type == "digiline" and event.channel ~= "touchscreen" then
    for unit_channel, data in pairs(power_units) do
        if event.channel == unit_channel then
            data[2] = event.msg.supply
        end
    end

    cells = {"#ffffff", "Channel", "Name", "Power", "Status", "#888888", "------------------------", "----------------------------", "------------------------", "----------------"}

    local total_units = 0
    local total_active = 0
    local total_supply = 0
    local total_capacity = 0
    for unit_channel, data in pairs(power_units) do
       local status = ""
       local row_color = "#ffffff"
       total_units = total_units + 1
       total_capacity = total_capacity + data[3]
       if data[2] == 0 then
           status = "Stopped"
           row_color = "#ff0000"
       elseif 0 < data[2] and data[2] < data[3] then
           total_active = total_active + 1
           total_supply = total_supply + data[2]
           status = "Awake"
           row_color = "#ffff00"
       elseif data[2] == data[3] then
           total_active = total_active + 1
           total_supply = total_supply + data[2]
           status = "Active"
           row_color = "#00ff00"
       else
           status = "Error"
           row_color = "#880000"
       end
       unit_row = {row_color, unit_channel, data[1], (tostring(data[2]) .. " / " .. tostring(data[3])), status}
       cells = tableConcat(cells, unit_row)
    end

    workspace = {
    {
    command = "clear",
    },
    {
        command = "add",
        element = "label",
        X = 0,
        Y = 0,
        label = "Power Units Management",
    },
    {
        command = "add",
        element = "table",
        X = 0,
        Y = 1,
        W = 7,
        H = 7,
        name = "table",
        cells = cells, --{"a", "b", "c", "d", "e", "f", "g", "h"},
        selected_id = 0,
        color = "#ffffff",
        background = "#111111",
        border = true,
        highlight = "#466432",
        highlight_text = "#ffffff",
        opendepth = 0,
        columns = {
            {type = "color"},
            {type = "text", width = "10"},
            {type = "text", width = "12"},
            {type = "text", width = "10"},
            {type = "text", width = "6"},
        },
    },
    {
        command = "add",
        element = "label",
        X = 7.25,
        Y = 1,
        label = "Total units : " .. total_units,
    },
    {
        command = "add",
        element = "label",
        X = 7.25,
        Y = 1.5,
        label = "Units active : " .. total_active,
    },
    {
        command = "add",
        element = "label",
        X = 7.25,
        Y = 2,
        label = "Supply : " .. total_supply .. " / " .. total_capacity,
    },
    }

    digiline_send("touchscreen", workspace)

end
