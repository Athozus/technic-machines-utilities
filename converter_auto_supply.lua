local update_time = 2 -- time for loop
local converter_channel = "converter" -- supply converter channel
local switch_channel = "switch" -- switching station channel
local efficiency = 0.9 -- 10 per cent lost while converting
local force_enable = false -- power supply converter when disabled

if event.type == "program" then
	-- start interrupt cycle
	interrupt(update_time)
end

if event.type == "interrupt" then
	-- request infos
	digiline_send(switch_channel, "get")
end

if event.type == "digiline" and event.channel == switch_channel then
    -- calculate input power based on efficiency
    demand = event.msg.demand
    supply = math.ceil(demand/efficiency)
    -- enable converter if disabled and autorized to
    if force_enable and demand > 0 then
        digiline_send(converter_channel, "on")
    end
    -- send to converter
    digiline_send(converter_channel, "power " .. supply)
    -- continue loop, add lag to prevent overheating
    if event.msg.lag then
        interrupt(update_time + event.msg.lag)
    else
        interrupt(update_time)
    end
end
